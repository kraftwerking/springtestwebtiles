<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    

Authorized users only!


<table class="formtable">
<tr><td>Username</td><td>Email</td><td>Role</td><td>Enabled</td></tr>
<c:forEach items="${users}" var="user">
<tr><td><c:out value="${user.username}"></c:out></td><td><c:out value="${user.email}"></c:out></td><td><c:out value="${user.authority}"></c:out></td><td><c:out value="${user.enabled}"></c:out></td></tr>

</c:forEach>

</table>
