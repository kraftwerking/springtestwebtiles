<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


	<sf:form method="post"
		action="${pageContext.request.contextPath}/createaccount"
		commandName="user"
		id="details">
		<table class="formtable">
			<tr>
				<td class="label">Username:</td>
				<td><sf:input path="username" name="username" type="text" /><br>
				<div class="error"><sf:errors path="username"></sf:errors></div></td>
			</tr>
			<tr>
				<td class="label">Name:</td>
				<td><sf:input path="name" name="name" type="text" /><br>
				<div class="error"><sf:errors path="name"></sf:errors></div></td>
			</tr>
			<tr>
				<td class="label">Email:</td>
				<td><sf:input path="email" name="email" type="text" /><br>
				<div class="error"><sf:errors path="email"></sf:errors></div></td>
			</tr>
			<tr>
				<td class="label">Password:</td>
				<td><sf:input path="password" id="password" name="password" type="text" /><br>
				<div class="error"><sf:errors path="password"></sf:errors></div></td>
			</tr>
			<tr>
				<td class="label">Confirm Password:</td>
				<td><input name="confirmpass" id="confirmpass"  type="text"><div id="matchpass"></div></td><br>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input class="control" value="Create account" type="submit" /></td>
			</tr>
		</table>

	</sf:form>

