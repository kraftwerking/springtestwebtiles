<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<a class="title" href="<c:url value='/'/>">Offers homepage</a>

<sec:authorize access="isAuthenticated()">
	<p>
		<a class="login" href="<c:url value='/j_spring_security_logout'/>">Log out</a>
	</p>
</sec:authorize>
<sec:authorize access="!isAuthenticated()">
	<p>
		<a class="login" href="<c:url value='/login'/>">Log in</a>
	</p>
</sec:authorize>