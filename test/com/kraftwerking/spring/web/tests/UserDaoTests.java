package com.kraftwerking.spring.web.tests;

import static org.junit.Assert.*;

import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.kraftwerking.spring.web.dao.User;
import com.kraftwerking.spring.web.dao.UsersDao;

@ActiveProfiles("dev")
@ContextConfiguration(locations = {
		"classpath:com/kraftwerking/spring/web/config/dao-context.xml",
		"classpath:com/kraftwerking/spring/web/config/security-context.xml",
		"classpath:com/kraftwerking/spring/web/test/config/datasource.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class UserDaoTests {
	
	@Autowired
	private UsersDao usersDao;
	
	@Autowired
	private DataSource dataSource;

	private User user1 = new User("johnjohnjohn", "John", "hellothere",
			"john@kraftwerking.com", 1, "ROLE_USER");
	private User user2 = new User("richardhannay", "Richard Hannay", "the39steps",
			"richard@kraftwerking.com", 1, "ROLE_ADMIN");
	private User user3 = new User("suetheviolinist", "Sue Black", "iloveviolins",
			"sue@kraftwerking.com", 1, "ROLE_USER");
	private User user4 = new User("rogerblake", "Rog Blake", "liberator",
			"rog@kraftwerking.com", -1, "user");

	@Before
	public void init() {
		JdbcTemplate jdbc = new JdbcTemplate(dataSource);
		
		jdbc.execute("delete from users");
	}
	
	@Test
	public void testCreatRetrieve(){
		usersDao.create(user1);
		List<User> users1 = usersDao.getAllUsers();
		assertEquals("One user should have been created and retrieved.", 1, users1.size());
		assertEquals("Asserted user should have been retrieved.", user1, users1.get(0));
		usersDao.create(user2);
		usersDao.create(user3);
		usersDao.create(user4);
		List<User> users2 = usersDao.getAllUsers();
		assertEquals("There should be four users retrieved.", 4, users2.size());


	}
	
	//TODO: reimplement this
	@Test
	public void testCreateUser() {
		User user = new User("red", "redred", "hellothere", "red@kraftwerking.com", 1, "user");
		
		usersDao.create(user);
		
		List<User> users = usersDao.getAllUsers();
		
		assertEquals("Number of users should be 1.", 1, users.size());
		
		assertTrue("User should exist.", usersDao.exists(user.getUsername()));
		
		assertFalse("User should not exist.", usersDao.exists("sdfsdfgsdg"));
		
		//assertEquals("Created user should be identical to retrieved user", user, users.get(0));
	}
	
	@Test
	public void testExists(){
		usersDao.create(user1);
		usersDao.create(user2);
		usersDao.create(user3);
		
		assertTrue("User should exist.", usersDao.exists(user1.getUsername()));
		assertFalse("User should not exist.", usersDao.exists("sdfsdfgsdg"));
	}
	
}
