package com.kraftwerking.spring.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import com.kraftwerking.spring.web.dao.Offer;
import com.kraftwerking.spring.web.dao.OffersDao;

@Service("offersService")
public class OffersService {

	private OffersDao offersDAO;

	@Autowired
	public void setOffersDAO(OffersDao offersDAO) {
		this.offersDAO = offersDAO;
	}

	public List<Offer> getCurrent() {
		return offersDAO.getOffers();

	}


	public boolean hasOffer(String name) {
		if (name == null) {
			return false;
		}

		List<Offer> offers = offersDAO.getOffers(name);
		if (offers.size() == 0) {
			return false;
		}
		return true;
	}

	public Offer getOffer(String username) {
		if (username == null) {
			return null;
		}
		List<Offer> offers = offersDAO.getOffers(username);
		if (offers.size() == 0) {
			return null;
		}
		return offers.get(0);
	}

	public List<Offer> getOffers(String username) {
		if (username == null) {
			return null;
		}
		List<Offer> offers = offersDAO.getOffers(username);
		if (offers.size() == 0) {
			return null;
		}
		return offers;
	}

	public void saveOrUpdate(Offer offer) {
		if (offer.getId() != 0){
			offersDAO.saveOrUpdate(offer);
		} else {
			offersDAO.saveOrUpdate(offer);
		}
		
	}

	public void delete(int id) {
		offersDAO.delete(id);
		
	}
}
