CREATE DATABASE  IF NOT EXISTS `springtest` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `springtest`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: localhost    Database: springtest
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authorities`
--

DROP TABLE IF EXISTS `authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorities` (
  `username` varchar(60) NOT NULL,
  `authority` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorities`
--

LOCK TABLES `authorities` WRITE;
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
INSERT INTO `authorities` VALUES ('redmilitante','ROLE_USER');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offers`
--

DROP TABLE IF EXISTS `offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offers`
--

LOCK TABLES `offers` WRITE;
/*!40000 ALTER TABLE `offers` DISABLE KEYS */;
INSERT INTO `offers` VALUES (1,'Bob','bob@nowhereatall.com','I will write Java for you'),(2,'Mike','mike@nowhereatall.com','Web design, very cheap'),(3,'Sue','sue@nowhereatall.com','PHP coding'),(4,'coding java','Dave','dave@kraftwerking.com'),(5,'testing software to order','Karen','karen@kraftwerking.com'),(6,'coding java','Dave','dave@kraftwerking.com'),(7,'testing software to order','Karen','karen@kraftwerking.com'),(8,'coding java','Dave','dave@kraftwerking.com'),(9,'testing software to order','Karen','karen@kraftwerking.com'),(10,'Web design to fit any budget','Claire','claire@kraftwerking.com'),(11,'Dave','dave@kraftwerking.com','coding java'),(12,'Karen','karen@kraftwerking.com','testing software to order'),(13,'Claire','claire@kraftwerking.com','Web design to fit any budget'),(14,'Dave','dave@kraftwerking.com','coding java'),(15,'Karen','karen@kraftwerking.com','testing software to order'),(16,'Claire','claire@kraftwerking.com','Web design to fit any budget'),(17,'Dave','dave@kraftwerking.com','coding java'),(18,'Karen','karen@kraftwerking.com','testing software to order'),(19,'Claire','claire@kraftwerking.com','Web design to fit any budget'),(20,'Dave','dave@kraftwerking.com','coding java'),(21,'Karen','karen@kraftwerking.com','testing software to order'),(22,'Claire','claire@kraftwerking.com','Web design to fit any budget'),(23,'Dave','dave@kraftwerking.com','coding java'),(24,'Karen','karen@kraftwerking.com','testing software to order'),(25,'Claire','claire@kraftwerking.com','Web design to fit any budget'),(26,'Dave','dave@kraftwerking.com','Cash for programming'),(27,'Karen','karen@kraftwerking.com','Elegant web design'),(28,'Dave','dave@kraftwerking.com','coding java'),(29,'Karen','karen@kraftwerking.com','testing software to order'),(30,'Claire','claire@kraftwerking.com','Web design to fit any budget'),(31,'Dave','dave@kraftwerking.com','Cash for programming'),(32,'Karen','karen@kraftwerking.com','Elegant web design'),(33,'Dave','dave@kraftwerking.com','coding java'),(34,'Karen','karen@kraftwerking.com','testing software to order'),(35,'Claire','claire@kraftwerking.com','Web design to fit any budget'),(36,'Dave','dave@kraftwerking.com','Cash for programming'),(37,'Karen','karen@kraftwerking.com','Elegant web design'),(38,'Roger Moore','roger@creativestuff.com','I will create amazing websites for you.'),(39,'rerere','re@re.org','readffdasdfafdadsfadsfasdfasdfasdfasdfadsf');
/*!40000 ALTER TABLE `offers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `username` varchar(60) NOT NULL,
  `password` varchar(80) DEFAULT NULL,
  `enabled` int(11) DEFAULT '1',
  `email` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('redmilitante','4153b12ac15609cdc6f2e64e8c29143a32fca0a9b3a0970c0af22d6aad2874e1f6ac654355b36461',1,'red@red.org');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-14  9:22:02
